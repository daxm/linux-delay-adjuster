#! /usr/bin/env bash

# Set interface and delay variables
if [ $# -eq 0 ]
  then
    delay=500
    jitter=250
    interface=ens160
  else
    interface=$1
    delay=$2
    jitter=$3
fi

# See whether delay is already set on this interface.  If so, remove it.
result=`tc -s qdisc | grep delay`
if [ ${#result} -gt 0 ]
  then
    $(dirname "$0")/No_Delay.sh ${interface}
fi

# Set delay on interface.
echo -e "Adding delay of ${delay}ms with jitter of ${jitter}ms to outbound traffic on ${interface} interface."
sudo tc qdisc add dev ${interface} root netem delay ${delay}ms ${jitter}ms

# tc -s qdisc | grep ${interface}
