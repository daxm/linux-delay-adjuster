# Linux Delay Adjuster

Add/remove delay on a NIC in your Linux box.  Why?  Because sometimes you have to make things worse in order to make things better.

## Prerequisites

1. Enable IP Routing on Linux server.

  - Edit **/etc/sysctl.conf** and uncomment **net.ipv4.ip_forward=1**
  - Reboot
  - Verify with `cat /proc/sys/net/ipv4/ip_forward` (should return/display "1".)

1. Configure NICs.

  - Modify /etc/netplan/00-installer-config.yaml to meet your networking needs.
  - Use `sudo netplan apply` to apply your changes.

1. Ubuntu comes with 'tc' already installed.  This is the core command for adding/removing delay with the scripts below.  You may have to install it in your distribution.

## How to Use

### Add_Delay.sh
Run the command and pass the name of the interface and how many milliseconds (ms) of delay you want injected.
`Add_Delay.sh <interface name> <delay int>`

> This script calls No_Delay.sh to clear any existing delay settings on this interface prior to setting the new value.  To my knowledge there is no "update" ability for setting delay.

Defaults:
  - interface = ens160 (why? because that works for me right now.)
  - delay = 500 (why? because that works for me right now.)

### No_Delay.sh
Run the command and pass the name of the interface that you wish to remove all delay settings.
`No_Delay.sh <interface name>`

Defaults:
  - interface = ens160 (why? because that works for me right now.)
