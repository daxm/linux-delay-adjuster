#! /usr/bin/env bash

# Set interface variable
if [ $# -eq 0 ]
  then
    interface=ens160
  else
    interface=$1
fi

echo -e "Removing any previously configured delay and jitter."
sudo tc qdisc del dev ${interface} root netem

# tc -s qdisc | grep ${interface}
